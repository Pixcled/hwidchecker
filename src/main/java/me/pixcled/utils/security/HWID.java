package me.pixcled.utils.security;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class HWID {
    private static String getMAC() throws Exception {
        InetAddress ip;
        ip = InetAddress.getLocalHost();
        NetworkInterface network = NetworkInterface.getByInetAddress(ip);
        byte[] mac = network.getHardwareAddress();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mac.length; i++) {
            sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
        }
        return sb.toString();
    }

    public static String getHWID() throws Exception {
        return encryptWithSHA1(String.valueOf(System.getenv("PROCESSOR_IDENTIFIER")) + System.getenv("COMPUTERNAME") + System.getProperty("user.name") + getMAC());
    }

    private static String encryptWithSHA1(final String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        final MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] sha1hash = new byte[40];
        md.update(text.getBytes("iso-8859-1"), 0, text.length());
        sha1hash = md.digest();
        return convertToHex(sha1hash);
    }

    private static String convertToHex(final byte[] data) {
        final StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; ++i) {
            int halfbyte = data[i] >>> 4 & 0xF;
            int two_halfs = 0;
            do {
                if (halfbyte >= 0 && halfbyte <= 9) {
                    buf.append((char) (48 + halfbyte));
                } else {
                    buf.append((char) (97 + (halfbyte - 10)));
                }
                halfbyte = (data[i] & 0xF);
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static void checkHWID() {
        try {
            final URL url = new URL("https://dev.arcadiamc.net/hwidchecker/api/hwid.php?id=" + getHWID());
            System.setProperty("http.agent", "Chrome");
            final ArrayList<Object> lines = new ArrayList<>();
            final URLConnection connection = url.openConnection();
            final BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                lines.add(line);
            }
            if (!lines.contains("true")) {
                System.out.println("HWID > Error, this computer's HWID could not be found in ArcadiaMC's HWID database! Please try again later or contact a developer for support.");
                System.out.println("HWID > Your HWID: " + getHWID());
                System.exit(0);
            }
        } catch (Exception e) {
            System.out.println("HWID > Error, could not connect to ArcadiaMC's HWID database! Please try again later or contact a developer for support.");
            if (e.getMessage().contains("Connection timed out")) {
                System.out.println("Exception > Connection timed out! (java.net.ConnectionException)");
            } else if (e.getMessage().contains("arcadiamc.net")) {
                System.out.println("Exception > Connection failed! (java.net.UnknownHostException)");
            } else {
                System.out.println("Exception > Unknown exception type - please contact a developer!");
            }
            System.exit(0);
        }
    }
}
